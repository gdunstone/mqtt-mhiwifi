'use strict'

const acwm = require("./acwm-api/acwm-api.js")

const MQTT_TOPIC = "hvac/#name"
const MQTT_COMMAND_TOPIC = `cmnd/${MQTT_TOPIC}`
const MQTT_STATE_TOPIC = `stat/${MQTT_TOPIC}`
const MQTT_DATA_TOPIC = `${MQTT_STATE_TOPIC}/data`
const MQTT_CONFIG_TOPIC = `${MQTT_STATE_TOPIC}/config`
const MQTT_INFO_TOPIC = `${MQTT_STATE_TOPIC}/info`

const argv = require('yargs')
    .usage('Usage: $0 [--discover] --interval [interval minutes] --mqtt [mqtt url] [--mqtt-user "mqtt user"] [--mqtt-password "mqtt password"] [--ips ip address(,ip address,...)]')
    .argv;



let ip_string = argv.ips || process.env.IPS;

var supplied_intesis_ips = ip_string.split(',');
let interval = parseInt((argv.interval || process.env.INTERVAL || "10"), 10);


const mqtt_url = argv.mqtt || process.env.MQTT_URL || "mqtt://mqtt:1883";
const mqtt_password = argv.mqtt_password || process.env.MQTT_PASSWORD;
const mqtt_user = argv.mqtt_user || process.env.MQTT_USER;


var winston = require('winston');

const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.combine(
        winston.format.splat(),
        winston.format.simple()
    ),
    transports: [
        new winston.transports.Console()
    ]
});

const mqtt = require('mqtt');

//todo detect connection failures
let mqtt_connect_opts = {
    connectTimeout: 5*1000,
};
if (mqtt_user && mqtt_password){
    mqtt_connect_opts.username = mqtt_user;
    mqtt_connect_opts.password = mqtt_password;
}

logger.info(`Connecting to ${mqtt_url}`);
let mqttClient = mqtt.connect(mqtt_url, mqtt_connect_opts)
mqttClient.on('error', function (error) {
    logger.error("Error from mqtt broker: %v", error)
});
mqttClient.on('connect', function (connack) {
    mqttClient.subscribe(MQTT_COMMAND_TOPIC.replace("#name", "#"), (err)=>{
        if (err) logger.error(err);
    });
    logger.info("Connected to mqtt broker")
});


var aircons = {};


supplied_intesis_ips.forEach(function(ip){
    let airco = new acwm(ip, "admin", "admin");
    airco.init().then(()=>{
        airco.getInfo().then((info)=>{
            logger.info(`${ip}=> ${info.wlanAPMAC} init success`);
            aircons[info.wlanAPMAC.replace(/\:/g, '')] = airco;
        })  
    });
});


var stateMap = {
    1: {
        "name": "power",
        "states": { 
                0: "off",
                1: "on"
            }   
    },
    2: {
        "name": "mode",
        "states": {
                0: "auto",
                1: "heat",
                2: "dry",
                3: "fan_only",
                4: "cool",
            }
    },
    4: {
        "name": "fanspeed",
        "states": {
                0: "auto",
                1: "1",
                2: "2",
                3: "3",
                4: "4",
                5: "5",
                6: "6",
                7: "7",
                8: "8",
                9: "9",
                10: "10",
            }
    },
    5: {
        "name": "swing",
        "states": {
                0: "auto",
                1: "pos1",
                2: "pos2",
                3: "pos3",
                4: "pos4",
                5: "pos5",
                6: "pos6",
                7: "pos7",
                8: "pos8",
                9: "pos9",
                10: "swing",
                11: "swirl",
                12: "wide",
            }
    }
}


var runMqttPublish = function(){
    for (let [key,aircon] of Object.entries(aircons)){
        aircon.getInfo().then((info)=>{
            let infotop = MQTT_INFO_TOPIC.replace("#name", key);
            logger.debug(`publishing to ${infotop}`);
            mqttClient.publish(infotop, JSON.stringify(info));
        })

        // aircon.getCurrentConfig().then((data)=>{
        //     let configtop = MQTT_CONFIG_TOPIC.replace("#name", key)
        //     logger.info(`publishing to ${configtop}`)
        //     mqttClient.publish(configtop, JSON.stringify(data));
        // })
        aircon.getDataPointValue().then((data)=>{
            let datatop = MQTT_DATA_TOPIC.replace("#name", key);
            logger.debug(`publishing to ${datatop}`);
            mqttClient.publish(datatop, JSON.stringify(data));
            let stat_top = MQTT_STATE_TOPIC.replace("#name", key);
            let uidtovalue = {}
            for (let value of data) {
                uidtovalue[value.uid] = value.value;
            }


            for ( let [k, o] of Object.entries(stateMap)) {

                let t = `${stat_top}/${o.name}`;
                let v = uidtovalue[k];
                let s = o.states[v];
                // if power is off, report mode as off as well.
                if (uidtovalue[1] == 0 && k == 2){
                    s = "off";   
                }
                
                mqttClient.publish(t, s);
            }
            mqttClient.publish(`${stat_top}/temperature`, 
                uidtovalue[9].toString());
            mqttClient.publish(`${stat_top}/return_path_temperature`, 
                uidtovalue[10].toString());
            mqttClient.publish(`${stat_top}/outdoor_temperature`, 
                uidtovalue[37].toString()); 
        })
    }
    setTimeout(runMqttPublish, interval*1000)
}
runMqttPublish();


var vMap = {
    "power": {
        "off": 0,
        "on": 1,    
    },
    "mode": {
        "off": -1,
        "auto": 0,
        "heat": 1,
        "dry": 2,
        "fan_only": 3,
        "cool": 4,
    },
    "fanspeed": {
        "auto": 0,
        "1": 1,
        "2": 2,
        "3": 3,
        "4": 4,
        "5": 5,
        "6": 6,
        "7": 7,
        "8": 8,
        "9": 9,
        "10": 10,
    },
    "swing": {
        "auto": 0,
        "pos1": 1,
        "pos2": 2,
        "pos3": 3,
        "pos4": 4,
        "pos5": 5,
        "pos6": 6,
        "pos7": 7,
        "pos8": 8,
        "pos9": 9,
        "swing": 10,
        "swirl": 11,
        "wide": 12
    }
}


mqttClient.on('message', 
    function(topic, message) {
        // parse device id (mac)
        // topic eg: cmnd/hvac/CE3F1D021D4E/mode/set
        let topicChunks = topic.split("/");
        let devid = topicChunks[2];
        // if we dont have a dev, exit
        if (!(devid in aircons)) return;
        // we only care about set ops
        if (topicChunks[4] != "set") return;
        let op = topicChunks[3];
        let uid = -1;
        let value = -1;

        let lmsg = String(message).toLowerCase();
        switch (op){
            case "power":
                uid = 1;
                value = vMap[op][lmsg];
                break;
            case "mode":
                if (lmsg == "off") {
                    uid =1
                    op = "power"
                    value = vMap[op][lmsg];
                } else {
                    uid = 2;
                    value = vMap[op][lmsg];
                }
                break;
            case "fanspeed":
                uid = 4;
                value = vMap[op][lmsg];
                break;
            case "swing":
                uid = 5;
                value = vMap[op][lmsg];
                break;
            case "temperature":
                uid = 9;
                value = parseInt(message, 10)*10;
                break;
        }

        if (uid < 0) return;
        let data = {
            "uid": uid,
            "value": value
        }
        aircons[devid].writeCommand("setdatapointvalue", data).then(function(){
            logger.info(`${devid} ran command, ${op}:${message} ${uid}:${value}`);
            let stat_top = MQTT_STATE_TOPIC.replace("#name", devid);
            mqttClient.publish(`${stat_top}/${op}`, lmsg);

            if (op == "mode" && lmsg != "off"){
                let op_temp = "power"
                let data_temp = {
                    "uid": 1,
                    "value": 1
                }
                aircons[devid].writeCommand("setdatapointvalue", data_temp).then(function(){
                    logger.info(`${devid} ran command, power:on 1:1`);
                    let stat_top = MQTT_STATE_TOPIC.replace("#name", devid);
                    mqttClient.publish(`${stat_top}/${op_temp}`, "on");
                });
            }
        });
    }
)